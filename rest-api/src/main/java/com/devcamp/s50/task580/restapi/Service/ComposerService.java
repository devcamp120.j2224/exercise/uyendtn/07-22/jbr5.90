package com.devcamp.s50.task580.restapi.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.devcamp.s50.task580.restapi.model.Artist;
import com.devcamp.s50.task580.restapi.model.Band;
import com.devcamp.s50.task580.restapi.model.Bandmember;
import com.devcamp.s50.task580.restapi.model.Composer;

public class ComposerService {
    @Autowired
    AlbumService albumService;
    Artist artist1 = new Artist("Adele", "Hudwalker", "Adele", albumService.getAlbumList() );
    Artist artist2 = new Artist("Amy", "Hudwalker", "Lady Gaga", albumService.getAlbumList() );
    Artist artist3 = new Artist("Adam", "Larvine", "Adam", albumService.getAlbumList() );

    Bandmember member1 = new Bandmember("Adam", "piano");
    Bandmember member2 = new Bandmember("Amy", "drummer");
    Bandmember member3 = new Bandmember("Gita", "guirta");
    Bandmember member4 = new Bandmember("Simon", "trumpet");
    Bandmember member5 = new Bandmember("Juan", "vocalist");
    Bandmember member6 = new Bandmember("Tony", "electric guirta");

    public List<Composer> getComposerList() {
        List<Composer> composerList = new ArrayList<>();
        composerList.add(artist1);
        composerList.add(artist2);
        composerList.add(artist3);
        composerList.add(member1);
        composerList.add(member2);
        composerList.add(member3);
        composerList.add(member4);
        composerList.add(member5);
        composerList.add(member6);

        return composerList;

    }

    public List<Band> getBandList() {
        List<Bandmember> band1Member = new ArrayList<>();
        band1Member.add(member1);
        band1Member.add(member2);
        List<Bandmember> band2Member = new ArrayList<>();
        band2Member.add(member3);
        band2Member.add(member4);

        Band band1 = new Band("RocknRoll", band1Member, albumService.getAlbumList());
        Band band2 = new Band("BTS", band2Member, albumService.getAlbumList());
        List<Band> bandList = new ArrayList<>();
        bandList.add(band1);
        bandList.add(band2);
        return bandList;
    }
}
