package com.devcamp.s50.task580.restapi.model;

public class Bandmember extends Composer {
    private String instrument;

    public Bandmember() {
    }

    public Bandmember(String instrument) {
        this.instrument = instrument;
    }

    public Bandmember(String stagename, String instrument) {
        super(stagename);
        this.instrument = instrument;
    }

    public Bandmember(String firstname, String lastname, String stagename, String instrument) {
        super(firstname, lastname, stagename);
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    
}
